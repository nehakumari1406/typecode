import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { LanguageModule } from './language/language.module';
import { CommentsModule } from './Comments/Comments.module';
import { UsersModule } from './users/users.module';
import { PostsModule } from './posts/posts.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as Sentry from '@sentry/node';
import * as mongoose from 'mongoose';
import * as dotenv from 'dotenv';


const os = require('os');
import * as bodyParser from 'body-parser';

async function bootstrap() {
  dotenv.config();
  const app = await NestFactory.create(AppModule, {
    bodyParser: false
  });

  const { env } = process;
  if (env.NODE_ENV === "dev") {
    mongoose.set('debug', true)
  }

  const rawBodyBuffer = (req, res, buf, encoding) => {
    if (buf && buf.length) {
      req.rawBody = buf.toString(encoding || 'utf8');
    }
  };

  app.use(bodyParser.json({ limit: '500mb' }));
  app.use(bodyParser.urlencoded({ limit: '500mb', extended: true }));

  app.use(bodyParser.urlencoded({ verify: rawBodyBuffer, extended: true }));
  app.use(bodyParser.json({ verify: rawBodyBuffer }));
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();

  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Accept');
    next();
  });

  if (process.env.NODE_ENV === 'production') Sentry.init({ dsn: "https://9c84670941d7445db564e262769d1916@o1091520.ingest.sentry.io/6110200", tracesSampleRate: 1.0, });

  if (process.env.PREDIFINED && process.env.PREDIFINED == "true") {
    let options = new DocumentBuilder().setTitle('typecode').setBasePath("/").setVersion('v1').addBearerAuth().setSchemes('https', 'http').build();

    const document = SwaggerModule.createDocument(app, options, {
      include: [LanguageModule, PostsModule, CommentsModule, UsersModule]
    });
    SwaggerModule.setup('/explorer', app, document);
  }
  const port = process.env.PORT || 3000;
  await app.listen(port);
  console.log(`http://localhost:${port}/explorer/#/`)
}

bootstrap();

