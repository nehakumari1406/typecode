import { Controller, Get, Post, Body, Param, UseGuards, Query } from '@nestjs/common';

import { ResponseMessage, CommonResponseModel, AdminQuery } from '../auth/app.model';
import { UtilService } from '../auth/util.service';
import { AuthGuard } from '@nestjs/passport';

import { ApiUseTags, ApiBearerAuth, } from '@nestjs/swagger';

import { GetUser } from '../auth/jwt.strategy';
import { PostsService } from './posts.service';
import { PostsDto } from './posts.model';
import { UsersDTO } from '../users/Users.model';


@Controller('posts')
@ApiUseTags('Posts')
export class PostsController {
    constructor(
        private postService: PostsService,
        private utilService: UtilService) { }

    @Post('/create-user')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    public async create(@GetUser() user: UsersDTO, @Body() creatPostData: PostsDto) {
        this.utilService.validateAdminRole(user);
        try {
            const commentExist = await this.postService.getPosts(creatPostData.title);
            console.log(commentExist)
            if (commentExist) this.utilService.badRequest(ResponseMessage.MASTER_CATEGORY_ALREADY_EXIST);
            creatPostData.userId = user._id;
            const comment = await this.postService.createPosts(creatPostData);
            console.log(comment)
            if (comment) return this.utilService.successResponseData(ResponseMessage.COMMENT_SAVED);
            else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
        }
        catch (e) {
            console.log(e)
            this.utilService.errorResponse(e);
        }

    }

    @Get('/getallposts')
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    public async commentsallList(@GetUser() user: UsersDTO, @Query() adminQuery: AdminQuery): Promise<CommonResponseModel> {
        this.utilService.validateAdminRole(user);


        try {
            let query = this.utilService.getAdminQuery(adminQuery);
            let orderFilter = {};


            const data = await Promise.all([
                this.postService.postList(query.page, query.limit, query.q, query.userId),
                this.postService.countPostList(query.q, query.userId)
            ])
            return this.utilService.successResponseData(data[0], { total: data[1] });
        } catch (e) {
            this.utilService.errorResponse(e);
        }
    }

    @Get('/getposts/:id')
    public async getCategorynfo(@Param('id') id: number) {

        try {
            const data = await this.postService.getPostsDetail(id);
            if (data) return this.utilService.successResponseData(data);
            else this.utilService.badRequestResponseData(ResponseMessage.COMMENTS_NOT_FOUND);
        } catch (e) {
            this.utilService.errorResponse(e);
        }
    }


}
