import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';

import { POstSchema } from './posts.model';


@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Post', schema: POstSchema }]),
    ],
    controllers: [PostsController],
    providers: [PostsService, MongooseModule]
})
export class PostsModule { }




