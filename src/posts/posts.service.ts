import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PostsDto } from './posts.model';




@Injectable()
export class PostsService {
    constructor(

        @InjectModel('Post') private masterPostModel: Model<any>) {
    }


    public async createPosts(createPostData: PostsDto): Promise<any> {
        const menu = await this.masterPostModel.create(createPostData);
        return menu;
    }
    public async getPosts(title: string): Promise<any> {

        return await this.masterPostModel.findOne({ title: title });
    }

    public async postList(page: number, limit: number, search?: string, userId?: string): Promise<Array<any>> {
        const skip = Number(page - 1) * Number(limit);
        let filter = {};
        if (search) filter = { title: { $regex: search, $options: 'i' } }
        if (userId) filter['userId'] = userId;
        let post = await this.masterPostModel.find(filter).limit(limit).skip(skip);
        return post;
    }

    public async countPostList(search?: string, userId?: string): Promise<number> {
        let filter = {};
        if (search) filter = { title: { $regex: search, $options: 'i' } }
        if (userId) filter['userId'] = userId;
        return await this.masterPostModel.countDocuments(filter);
    }

    public async getPostsDetail(id: number): Promise<any> {
        return await this.masterPostModel.findById({ _id: id });
    }


    // update(id: number, updateCommentDto: UpdateCommentDto) {
    //   return `This action updates a #${id} comment`;
    // }

    remove(id: number) {
        return `This action removes a #${id} comment`;
    }
}
