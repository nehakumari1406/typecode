
import * as mongoose from 'mongoose';
import {
    IsNotEmpty,

} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export const POstSchema = new mongoose.Schema(
    {
        userId: { type: mongoose.Types.ObjectId, ref: 'User' },
        title: { type: String },
        body: { type: String },


    });
export class PostsDto {

    userId: string;

    @ApiModelProperty()
    @IsNotEmpty()
    title: string;

    @ApiModelProperty()
    @IsNotEmpty()
    body: string;



}
