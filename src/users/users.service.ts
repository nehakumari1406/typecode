import { Injectable, } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UsersDTO, VendorCreateDTO, CreateuserDto } from './users.model';
import { AuthService } from '../auth/auth.service';
import { UtilService } from '../auth/util.service';

@Injectable()
export class UserService {
    constructor(
        @InjectModel('User') private readonly userModel: Model<any>,
        private authService: AuthService,
        private utilService: UtilService,
    ) {

    }

    public async getUserByEmail(email: String): Promise<UsersDTO> {
        const user = await this.userModel.findOne({ email: email });
        return user;
    }
    public async userList(page: number, limit: number, search?: string): Promise<Array<any>> {
        const skip = Number(page - 1) * Number(limit);
        let filter = {};
        if (search) filter = { title: { $regex: search, $options: 'i' } }

        let comment = await this.userModel.find(filter).limit(limit).skip(skip);
        return comment;
    }
    public async countuserList(search?: string): Promise<number> {
        let filter = {};
        if (search) filter = { title: { $regex: search, $options: 'i' } }

        return await this.userModel.countDocuments(filter);
    }
    public async createusers(createuserData: VendorCreateDTO): Promise<any> {
        const menu = await this.userModel.create(createuserData);
        return menu;
    }
    public async createAdmin(createuserData: CreateuserDto): Promise<any> {
        const menu = await this.userModel.create(createuserData);
        return menu;
    }
    public async getusers(email: string): Promise<any> {

        return await this.userModel.findOne({ email: email });
    }
    public async getUserDeatilForJWTValidate(userId: string): Promise<UsersDTO> {
        const user = await this.userModel.findById(userId, 'name username email phone status website role');
        return user;
    }
}
