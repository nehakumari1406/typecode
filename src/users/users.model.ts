import * as mongoose from 'mongoose';
import {
    IsNotEmpty,
    IsEmail,
    IsEmpty,
    Length,
    IsOptional,
    IsString,
    IsEnum,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

import { UserRoles } from '../auth/app.model';


export enum ThemeColour {
    RED = 'RED',
    BLUE = 'BLUE',
    PINK = 'PINK'
}
export enum ViewType {
    LIST = 'LIST',
    GRID = 'GRID'
}
export enum OrderingMode {
    PRINTER = 'PRINTER',
    WATERAPP = 'WATERAPP',
}


export const UserSchema = new mongoose.Schema(
    {
        name: { type: String },
        username: { type: String },
        email: { type: String, trim: true, lowercase: true, sparse: true },
        role: { type: String },
        password: { type: String },
        status: { type: Boolean, default: true },
        address: {
            street: { type: String },
            suite: { type: String },
            city: { type: String },
            zipcode: { type: String },

            geo: {
                lat: { type: String },
                lng: { type: String }
            }
        },
        phone: { type: String },
        website: { type: String },
        company: {
            name: { type: String },

            catchPhrase: { type: String },
            bs: { type: String }
        }


    },

);






// ########## --- DTO's --- ##########
export class company {
    name: string
    catchPhrase: string
    bs: string
}
export class CompanyDTO {
    @ApiModelProperty()
    @IsString()
    name: string;

    @ApiModelProperty()
    @IsString()
    catchPhrase: string;

    @ApiModelProperty()
    @IsString()
    bs: string;


}
export class CompanyUpdateDTO {
    @ApiModelProperty()
    company: CompanyDTO

}

export class geo {
    lat: string
    lng: string

}
export class geoDTO {
    @ApiModelProperty()
    @IsString()
    lat: string;

    @ApiModelProperty()
    @IsString()
    lng: string;

}
export class GeoUpdateDTO {
    @ApiModelProperty()
    geo: geoDTO

}
export class LoginDTO {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsString()
    email: string;

    @ApiModelProperty()
    @IsNotEmpty()
    @Length(6, 35)
    @IsString()
    password: string;


}
export class address {
    street: string
    suite: string
    city: string
    zipcode: string

}
export class AddressDTO {
    @ApiModelProperty()
    @IsString()
    street: string;

    @ApiModelProperty()
    @IsString()
    suite: string;

    @ApiModelProperty()
    @IsString()
    city: string;

    @ApiModelProperty()
    @IsString()
    zipcode: string;

    @ApiModelProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;


}
export class AddressUpdateDTO {
    @ApiModelProperty()
    address: AddressDTO;



}
export class CreateuserDto {
    @ApiModelProperty()
    @IsNotEmpty()
    username: string;

    @ApiModelProperty()
    @IsNotEmpty()
    name: string;

    @IsString()
    @ApiModelProperty()
    @IsNotEmpty()
    @Length(6, 35)
    password: string;
    @IsString()
    @ApiModelProperty()
    @IsNotEmpty()
    @Length(10, 11)
    phone: string;

    @ApiModelProperty()
    @IsNotEmpty()
    email: string;
    @ApiModelProperty()
    @IsNotEmpty()
    website: string;


    role: string;

    @ApiModelProperty({ type: AddressDTO })
    @Type(() => AddressDTO)
    address: AddressDTO;
    @ApiModelProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;

    @ApiModelProperty({ type: CompanyDTO })
    @Type(() => CompanyDTO)
    company: CompanyDTO;
}
export class VendorCreateDTO {
    @IsEmail()
    @ApiModelProperty()
    @IsNotEmpty()
    email: string;

    @IsString()
    @ApiModelProperty()
    @IsNotEmpty()
    @Length(6, 35)
    password: string;

    @IsString()
    @ApiModelProperty()
    @IsNotEmpty()
    username: string;

    @IsString()
    @ApiModelProperty()
    @IsNotEmpty()
    website: string;

    @IsString()
    @ApiModelProperty()
    @IsNotEmpty()
    @Length(10, 11)
    phone: string;

    role: string;
    deliveryManagementBy: string;
    @ApiModelProperty({ type: AddressDTO })
    @Type(() => AddressDTO)
    address: AddressDTO;
    @ApiModelProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;


    @ApiModelProperty({ type: CompanyDTO })
    @Type(() => CompanyDTO)
    company: CompanyDTO;






    status: boolean;

}


export class UsersDTO {


    @IsEmpty()
    _id: string;

    @IsString()
    @IsOptional()
    @ApiModelProperty()
    name: string;

    @IsString()
    @IsOptional()
    @ApiModelProperty()
    username: string;

    @IsString()
    @IsOptional()
    @ApiModelProperty()
    email: string;

    @IsString()
    @IsNotEmpty()
    @Length(5, 35)
    @ApiModelProperty()
    password: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelProperty()
    phone: string;

    @IsString()
    @ApiModelProperty()
    website: string;
    @IsNotEmpty()
    @ApiModelProperty({ enum: Object.keys(UserRoles) })
    @IsEnum(UserRoles, {
        message: 'Role type must be one of these ' + Object.keys(UserRoles),
    })
    role: string;

    @ApiModelProperty({ type: AddressDTO })
    @Type(() => AddressDTO)
    address: AddressDTO;
    @ApiModelProperty({ type: geoDTO })
    @Type(() => geoDTO)
    geo: geoDTO;

    @ApiModelProperty({ type: CompanyDTO })
    @Type(() => CompanyDTO)
    company: CompanyDTO;
    status: boolean;


}

export class UsersUpdateDTO {
    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    name?: string;

    @IsString()
    @IsOptional()
    @ApiModelPropertyOptional()
    username?: string;

    @IsString()
    @IsOptional()
    @ApiModelProperty()
    email?: string;

    @IsString()
    @IsOptional()
    @ApiModelProperty()
    phone?: string;

    @IsString()
    @IsOptional()
    @ApiModelProperty()
    website?: string


}


