import { Controller, Post, Body, Get, Query, UseGuards } from '@nestjs/common';
import { LoginDTO, VendorCreateDTO, UsersDTO, CreateuserDto } from './users.model';
import { UserService } from './users.service';
import { AuthGuard } from '@nestjs/passport';
import { JwtService } from '@nestjs/jwt';
import { GetUser } from '../auth/jwt.strategy';
import { ApiUseTags, ApiBearerAuth, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { UtilService } from '../auth/util.service';
import { UserRoles, ResponseMessage, CommonResponseModel, AdminQuery, ResponseErrorMessage, ResponseBadRequestMessage, ResponseSuccessMessage } from '../auth/app.model';
import { AuthService } from '../auth/auth.service';


@Controller('users')
@ApiUseTags('Users')
export class UserController {
    constructor(
        private userService: UserService,
        private jwtService: JwtService,
        private authService: AuthService,
        private utilService: UtilService

    ) {
    }

    /* ################################################### NO AUTH ################################## */

    @Post('/login')
    public async validateUser(@Body() credential: LoginDTO): Promise<CommonResponseModel> {
        try {
            const user = await this.userService.getUserByEmail(credential.email);
            console.log("user", user);
            if (!user) this.utilService.badRequest(ResponseMessage.USER_EMAIL_NOT_FOUND);
            const token = await this.authService.generateAccessToken(user._id, user.role);
            return this.utilService.successResponseData({ token: token, id: user._id, role: user.role });
        } catch (e) {
            console.log(e);
            this.utilService.errorResponse(e);
        }
    }
    @Get('admin/getallusers')
    public async commentsallList(@Query() adminQuery: AdminQuery): Promise<CommonResponseModel> {
        try {
            let query = this.utilService.getAdminQuery(adminQuery);
            const user = await Promise.all([
                this.userService.userList(query.page, query.limit, query.q),
                this.userService.countuserList(query.q)
            ]);
            return this.utilService.successResponseData(user[0], { total: user[1] });
        } catch (e) {
            this.utilService.errorResponse(e);
        }
    }
    @Post('admin/create-vendor')
    @ApiOperation({ title: 'Create Vendor' })
    @ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
    @ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
    @ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    public async create(@GetUser() user: UsersDTO, @Body() createuserData: VendorCreateDTO): Promise<CommonResponseModel> {
        this.utilService.validateAdminRole(user);

        try {
            const commentExist = await this.userService.getusers(createuserData.email);
            console.log(commentExist)
            if (commentExist) this.utilService.badRequest(ResponseMessage.EMAIL_ALREADY_EXIST);
            createuserData.role = UserRoles.VENDOR;
            const comment = await this.userService.createusers(createuserData);
            console.log(comment)
            if (comment) return this.utilService.successResponseData(ResponseMessage.USER_SAVED);
            else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
        }
        catch (e) {
            console.log(e)
            this.utilService.errorResponse(e);
        }

    }
    @Post('/create-admin')
    @ApiOperation({ title: 'Create Vendor' })
    @ApiResponse({ status: 200, description: 'Success message', type: ResponseSuccessMessage })
    @ApiResponse({ status: 400, description: 'Bad request message', type: ResponseBadRequestMessage })
    @ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })

    public async admincreate(@GetUser() user: UsersDTO, @Body() createuserData: CreateuserDto): Promise<CommonResponseModel> {


        try {
            const commentExist = await this.userService.getusers(createuserData.email);
            console.log(commentExist)
            if (commentExist) this.utilService.badRequest(ResponseMessage.EMAIL_ALREADY_EXIST);
            createuserData.role = UserRoles.ADMIN;
            const comment = await this.userService.createAdmin(createuserData);
            console.log(comment)
            if (comment) return this.utilService.successResponseData(ResponseMessage.USER_SAVED);
            else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
        }
        catch (e) {
            console.log(e)
            this.utilService.errorResponse(e);
        }

    }

}

