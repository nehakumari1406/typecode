
import {
    IsNumber,
    IsOptional,
    IsString,
} from 'class-validator';


import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export interface CommonResponseModel {
    response_code: number;
    response_data: any;
    extra?: string;
}

export class PaginationQuery {
    @IsOptional()
    page?: number;

    @IsOptional()
    limit?: number;
}
export enum UserSettings {
    DEFAULT_PAGE_NUMBER = 1,
    DEFAULT_PAGE_LIMIT = 10
}
export class AdminQuery {
    @IsOptional()
    @ApiModelPropertyOptional()
    page?: number;

    @IsOptional()
    @ApiModelPropertyOptional()
    limit?: number;
    @IsOptional()
    @ApiModelPropertyOptional()
    q?: string;
    @IsOptional()
    @ApiModelPropertyOptional()
    userId?: string;

}
export enum AdminSettings {
    DEFAULT_PAGE_NUMBER = 1,
    DEFAULT_PAGE_LIMIT = 10,
}

export class ResponseSuccessMessage {
    @IsString()
    @ApiModelProperty()
    response_code: string;

    @IsString()
    @ApiModelProperty()
    response_data: string;
}

export class ResponseBadRequestMessage {
    @IsNumber()
    @ApiModelProperty()
    status: number;

    @ApiModelProperty()
    errors: Array<string>;

}
export class ResponseErrorMessage {
    @IsNumber()
    @ApiModelProperty()
    status: number;

    @IsString()
    @ApiModelProperty()
    message: string;
}
export enum UserRoles {
    ADMIN = 'ADMIN',
    VENDOR = 'VENDOR',

}
export enum LanguageJsonType {
    BACKEND = 'backendJson',
    CMS = 'cmsJson',
    WEB = 'webJson',
    USER = 'mobAppJson',
    DELIVERY = 'deliveyAppJson',
    VENDOR = 'vendorAppJson'
}

export enum ResponseMessage {
    SOMETHING_WENT_WRONG = 'SOMETHING_WENT_WRONG',
    COMMENT_SAVED = 'COMMENT_SAVED',
    MASTER_CATEGORY_ALREADY_EXIST = 'MASTER_CATEGORY_ALREADY_EXIST',
    COMMENTS_NOT_FOUND = 'COMMENTS_NOT_FOUND',
    USER_EMAIL_NOT_FOUND = 'USER_EMAIL_NOT_FOUND',
    USER_ACCOUNT_BLOCKED = 'USER_ACCOUNT_BLOCKED',
    EMAIL_ALREADY_EXIST = 'EMAIL_ALREADY_EXIST',
    USER_EMAIL_NOT_VERIFIED = 'USER_EMAIL_NOT_VERIFIED',
    USER_EMAIL_OR_PASSWORD_INVALID = 'USER_EMAIL_OR_PASSWORD_INVALID',
    LANGUAGE_NOT_FOUND = 'LANGUAGE_NOT_FOUND',
    USER_SAVED = 'USER_SAVED',
    UNAUTHORIZED = 'UNAUTHORIZED'

}