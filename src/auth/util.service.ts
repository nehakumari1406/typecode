import {
    Injectable,
    HttpStatus,
    HttpException,
    BadRequestException,
    NotFoundException,
    UnauthorizedException,
    InternalServerErrorException
} from '@nestjs/common';
import * as uuid from 'uuid/v1';

import { ResponseMessage, UserSettings, PaginationQuery, AdminQuery, AdminSettings, UserRoles } from './app.model';

//import { } from '../order/order.service'
let language = "en";
let languageList = [];

@Injectable()
export class UtilService {
    private TIME_ZONE: string;
    constructor(

        // private orderService: OrderService
    ) {
        if (!process.env.TIME_ZONE) console.log("SET YOUR TIMEZONE OTHERWISE SOME FEATURES MAY BREAK OR NOT WORK PROPERLY");
        this.TIME_ZONE = process.env.TIME_ZONE || 'Asia/Kolkata';
    };




    //  VALIDATE ADMIN/MANAGER/VENDOR
    public validateAdminRole(user) {
        if (user.role !== UserRoles.ADMIN) {
            const msg = this.getTranslatedMessage(ResponseMessage.UNAUTHORIZED);
            throw new UnauthorizedException(msg);
        }
    }
    public validateVendorRole(user) {
        if (user.role !== UserRoles.VENDOR) {
            const msg = this.getTranslatedMessage(ResponseMessage.UNAUTHORIZED);
            throw new UnauthorizedException(msg);
        }
    }

    public getPaginationQuery(query: PaginationQuery) {
        return {
            page: Number(query.page) || UserSettings.DEFAULT_PAGE_NUMBER,
            limit: Number(query.limit) || UserSettings.DEFAULT_PAGE_LIMIT,

        };
    }
    public getAdminQuery(query: AdminQuery) {
        return {
            page: Number(query.page) || AdminSettings.DEFAULT_PAGE_NUMBER,
            limit: Number(query.limit) || AdminSettings.DEFAULT_PAGE_LIMIT,

            q: query.q || '',
            userId: query.userId || '',

        }
    }

    public async successResponseData(responseData, extra?) {
        if (!extra) return await this.res(HttpStatus.OK, responseData);
        let res = await this.res(HttpStatus.OK, responseData);
        for (var key in extra) res[key] = extra[key];
        return res;
    }

    public async successResponseMsg(key) {
        return await this.res(HttpStatus.OK, "", key);
    }

    public async badRequestResponseData(responseData?, extra?) {
        if (!extra) return await this.res(HttpStatus.BAD_REQUEST, responseData);
        let res = await this.res(HttpStatus.BAD_REQUEST, responseData);
        for (var key in extra) res[key] = extra[key];
        return res;
    }

    public async resetContentResponseMsg(key?) {
        return await this.res(HttpStatus.RESET_CONTENT, "", key);
    }

    public async processingResponseMsg(key?) {
        key = key || "PAYMENT_PROCESSING";
        return await this.res(HttpStatus.PROCESSING, "", key);
    }
    public async paymentRequiredResponseMsg(key?) {
        key = key || "PAYMENT_REQUIRED";
        return await this.res(HttpStatus.PAYMENT_REQUIRED, "", key);
    }

    public errorResponse(e) {
        console.log(e);
        if (e.kind === 'ObjectId' && e.path === '_id') {
            throw new NotFoundException("NOT_FOUND")
        }
        if (e.message && e.message.statusCode == HttpStatus.BAD_REQUEST) {
            throw new BadRequestException(e.message);
        }
        if (e.message && e.message.statusCode == HttpStatus.NOT_FOUND) {
            throw new NotFoundException(e.message.message);
        }
        throw new InternalServerErrorException(e.message);
    }

    public unauthorized() {
        const msg = this.getTranslatedMessage('UNAUTHORIZED');
        throw new UnauthorizedException(msg);
    }

    public badRequest(key?) {
        const msg = this.getTranslatedMessage(key);
        throw new BadRequestException(msg);
    }

    public pageNotFound() {
        const msg = this.getTranslatedMessage('NOT_FOUND');
        throw new NotFoundException(msg);
    }

    public async notFoundResponseMsg(key?) {
        key = key || "NOT_FOUND";
        return await this.res(HttpStatus.NOT_FOUND, "", key);
    }

    public async notFoundResponse(responseData, key?) {
        return await this.res(HttpStatus.NOT_FOUND, responseData);
    }

    public async internalErrorResponseKey(key?) {
        key = key || "INTERNAL_SERVER_ERR";
        return await this.res(HttpStatus.INTERNAL_SERVER_ERROR, "", key);
    }

    public async resetContentResponseKey(key) {
        return await this.res(HttpStatus.RESET_CONTENT, "", key);
    }

    public async resetContentResponseData(responseData) {
        return await this.res(HttpStatus.RESET_CONTENT, responseData);
    }

    public getTranslatedMessage(key) {
        let message = "";
        if (languageList && languageList[language] && languageList[language][key]) {
            message = languageList[language][key];
        } else {
            message = languageList["en"][key];
        }
        return message ? message : key;
    }
    public async getTranslatedMessageByKey(key) {
        let message = "";
        if (languageList && languageList[language] && languageList[language][key]) {
            message = languageList[language][key];
        } else {
            message = languageList["en"][key];
        }
        message = message || key;
        return message;
    }

    private async res(responseCode, responseData?, key?) {
        let message = "";
        if (responseData) {
            message = responseData;
        } else {
            if (languageList && languageList[language] && languageList[language][key]) {
                message = languageList[language][key];
            } else {
                message = languageList["en"][key];
            }
        }

        message = message || key;
        return {
            response_code: responseCode,
            response_data: message
        }
    }

    public async response(responseCode, responseData) {
        return {
            response_code: responseCode,
            response_data: responseData
        }
    }

    public setLanguage(lang: string) {
        language = lang;
    }

    public getLanguage() { return language; }

    public setLanguageList(list) {
        list.forEach(l => { languageList[l.languageCode] = l.backendJson; });
    }

    public getLanguageData(code) {
        return languageList[code];
    }

    public async getUUID() {
        return uuid();
    }


    public statusMessage(status, message) {
        return {
            status: status,
            data: message,
        };
    }

}

