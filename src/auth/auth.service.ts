import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';


@Injectable()
export class AuthService {
    constructor(
        private jwtService: JwtService
    ) {

    }

    // Generate salt for hashing password


    // Generate Token
    public async generateAccessToken(_id: string, role: string): Promise<string> {
        const payload = { _id, role };
        return this.jwtService.sign(payload, { expiresIn: '30d' });
    }
}
