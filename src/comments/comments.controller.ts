import { Controller, Get, Post, Body, Param, Delete, Query } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { ResponseMessage, CommonResponseModel, AdminQuery } from '../auth/app.model';
import { UtilService } from '../auth/util.service';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './comments.model';


@Controller('comments')
@ApiUseTags('Comments')
export class CommentsController {
  constructor(
    private commentsService: CommentsService,
    private utilService: UtilService) { }

  @Post('/create-comment')

  public async create(@Body() createCommentData: CreateCommentDto) {
    try {
      const commentExist = await this.commentsService.getComments(createCommentData.email);
      console.log(commentExist)
      if (commentExist) this.utilService.badRequest(ResponseMessage.MASTER_CATEGORY_ALREADY_EXIST);
      const comment = await this.commentsService.createComments(createCommentData);
      console.log(comment)
      if (comment) return this.utilService.successResponseData(ResponseMessage.COMMENT_SAVED);
      else this.utilService.badRequest(ResponseMessage.SOMETHING_WENT_WRONG);
    }
    catch (e) {
      console.log(e)
      this.utilService.errorResponse(e);
    }

  }

  @Get('/getallcomments')
  public async commentsallList(@Query() adminQuery: AdminQuery): Promise<CommonResponseModel> {
    try {
      let query = this.utilService.getAdminQuery(adminQuery);
      const comment = await Promise.all([
        this.commentsService.commentList(query.page, query.limit, query.q),
        this.commentsService.countCommentList(query.q)
      ]);
      return this.utilService.successResponseData(comment[0], { total: comment[1] });
    } catch (e) {
      this.utilService.errorResponse(e);
    }
  }

  @Get('/getcomment/:id')
  public async getCategorynfo(@Param('id') id: number) {

    try {
      const comment = await this.commentsService.getCommentsDetail(id);
      if (comment) return this.utilService.successResponseData(comment);
      else this.utilService.badRequestResponseData(ResponseMessage.COMMENTS_NOT_FOUND);
    } catch (e) {
      this.utilService.errorResponse(e);
    }
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateCommentDto: UpdateCommentDto) {
  //   return this.commentsService.update(+id, updateCommentDto);
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.commentsService.remove(+id);
  }
}
