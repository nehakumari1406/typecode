
import * as mongoose from 'mongoose';
import {
    IsNotEmpty,

} from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export const CommentSchema = new mongoose.Schema(
    {
        postId: { type: mongoose.Types.ObjectId, ref: 'Posts' },
        name: { type: String },
        body: { type: String },
        email: { type: String },


    });
export class CreateCommentDto {
    @ApiModelProperty()
    @IsNotEmpty()
    postId: string;

    @ApiModelProperty()
    @IsNotEmpty()
    name: string;

    @ApiModelProperty()
    @IsNotEmpty()
    body: string;

    @ApiModelProperty()
    @IsNotEmpty()
    email: string;


}
