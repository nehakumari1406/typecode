import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CommentsService } from './comments.service';
import { CommentsController } from './comments.controller';

import { CommentSchema } from './comments.model';


@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Comments', schema: CommentSchema }]),
  ],
  controllers: [CommentsController],
  providers: [CommentsService]
})
export class CommentsModule { }




