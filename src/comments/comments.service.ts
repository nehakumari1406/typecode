import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateCommentDto } from './comments.model';




@Injectable()
export class CommentsService {
  constructor(

    @InjectModel('Comments') private masterCommentsModel: Model<any>) {
  }


  public async createComments(createCommentData: CreateCommentDto): Promise<any> {
    const menu = await this.masterCommentsModel.create(createCommentData);
    return menu;
  }
  public async getComments(email: string): Promise<any> {

    return await this.masterCommentsModel.findOne({ email: email });
  }

  public async commentList(page: number, limit: number, search?: string): Promise<Array<any>> {
    const skip = Number(page - 1) * Number(limit);
    let filter = {};
    if (search) filter = { title: { $regex: search, $options: 'i' } }

    let comment = await this.masterCommentsModel.find(filter).limit(limit).skip(skip);
    return comment;
  }

  public async countCommentList(search?: string): Promise<number> {
    let filter = {};
    if (search) filter = { title: { $regex: search, $options: 'i' } }

    return await this.masterCommentsModel.countDocuments(filter);
  }

  public async getCommentsDetail(id: number): Promise<any> {
    return await this.masterCommentsModel.findById({ _id: id });
  }


  // update(id: number, updateCommentDto: UpdateCommentDto) {
  //   return `This action updates a #${id} comment`;
  // }

  remove(id: number) {
    return `This action removes a #${id} comment`;
  }
}
