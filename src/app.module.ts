import { Global, HttpException, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { CommentsModule } from './Comments/Comments.module';
import { PostsModule } from './posts/posts.module';
import { UsersModule } from './users/users.module';
import { LanguageModule } from './language/language.module';
import { AppController } from './app.controller';
import { JwtStrategy } from './auth/jwt.strategy';
import { UtilService } from './auth/util.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

import * as dotenv from 'dotenv';
import { RavenInterceptor } from 'nest-raven';

dotenv.config();

@Global()
@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: () => ({
        uri: (process.env.NODE_ENV == 'production') ? process.env.MONGO_DB_URL_PRODUCTION : process.env.MONGO_DB_URL_STAGING,
        useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true,
        useCreateIndex: true
      }),
    }),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({ secret: process.env.SECRET, signOptions: { expiresIn: '3h' } }),
    CommentsModule, PostsModule, LanguageModule, UsersModule,
    // PagesModule
  ],
  controllers: [AppController],
  providers: [
    UtilService, JwtStrategy,
    {
      provide: APP_INTERCEPTOR,

      useValue: new RavenInterceptor({
        filters: [
          // Filter exceptions of type HttpException. Ignore those that
          // have status code of less than 500
          { type: HttpException, filter: (exception: HttpException) => 500 > exception.getStatus() },
        ],
      }),
    },

  ],
  exports: [

    CommentsModule,
    PostsModule,
    LanguageModule,
    UsersModule,
    JwtModule,
    JwtStrategy,
    MongooseModule,
    UtilService,


  ]
})

export class AppModule {

}
