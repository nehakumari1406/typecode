import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { LanguageDTO, LanguageStatusUpdateDTO, } from './language.model';
import { LanguageJsonType } from '../auth/app.model';
@Injectable()
export class LanguageService {
    constructor(
        @InjectModel('Language') private readonly languageModel: Model<any>) {
    }



    public async getLanguageByCode(code: string): Promise<LanguageDTO> {
        let language = await this.languageModel.findOne({ languageCode: code });
        return language;
    }



    public async getDefaultLanguage(): Promise<LanguageDTO> {
        let language = await this.languageModel.findOne({ isDefault: 1 });
        return language;
    }



    // Update Language
    public async updateLanguage(languageId: string, languageData: LanguageDTO): Promise<LanguageDTO> {
        const language = await this.languageModel.findByIdAndUpdate(languageId, languageData, { new: true });
        return language;
    }



    // Set Default Language
    public async setDefaultLanguage(languageId: string): Promise<LanguageDTO> {
        await this.languageModel.updateMany({}, { isDefault: 0 });
        const language = await this.languageModel.findByIdAndUpdate(languageId, { isDefault: 1 }, { new: true });
        return language;
    }

    // Enable Disable Language
    public async languageStatusUpdate(languageId: string, languageStatusUpdateDTO: LanguageStatusUpdateDTO): Promise<LanguageDTO> {
        const language = await this.languageModel.findByIdAndUpdate(languageId, languageStatusUpdateDTO, { new: true });
        return language;
    }

    public async getLanguageForBackend(): Promise<Array<LanguageDTO>> {
        let language = await this.languageModel.find({}, 'languageCode languageName backendJson');
        return language;
    }

    // Get other language except en
    public async getOtherLanguage(): Promise<Array<LanguageDTO>> {
        let language = await this.languageModel.find({ languageCode: { $ne: "en" } });
        return language;
    }
    // language key-value update
    public async languageJsonUpdate(languageKeys, englishLanguage, jsonSourceKey, jsonType) {
        let jsonTargetKey = Object.keys(languageKeys[jsonType]);
        // console.log("jsonTargetKey", languageKeys.languageCode, jsonType);
        let allFounded = jsonSourceKey.filter(ai => !jsonTargetKey.includes(ai));
        if (allFounded && allFounded.length) {
            for (let keys of allFounded) {
                // console.log("allFounded", englishLanguage[jsonType][keys])
                languageKeys[jsonType][keys] = englishLanguage[jsonType][keys]
            }
        }
        return languageKeys[jsonType]
    }

    // update other language background process
    public async addKeyToOtherLanguage(englishLanguage): Promise<any> {
        let otherLanguages = await this.getOtherLanguage();
        let backendJsonSourceKey = Object.keys(englishLanguage.backendJson);
        let mobAppJsonSourceKey = Object.keys(englishLanguage.mobAppJson);
        let deliveyAppJsonSourceKey = Object.keys(englishLanguage.deliveyAppJson);
        let webJsonSourceKey = Object.keys(englishLanguage.webJson);
        let cmsJsonSourceKey = Object.keys(englishLanguage.backendJson);
        let vendorJsonSourceKey = Object.keys(englishLanguage.vendorAppJson);
        for (let languageKeys of otherLanguages) {
            let allJson = await Promise.all([
                this.languageJsonUpdate(languageKeys, englishLanguage, backendJsonSourceKey, LanguageJsonType.BACKEND),
                this.languageJsonUpdate(languageKeys, englishLanguage, mobAppJsonSourceKey, LanguageJsonType.USER),
                this.languageJsonUpdate(languageKeys, englishLanguage, deliveyAppJsonSourceKey, LanguageJsonType.DELIVERY),
                this.languageJsonUpdate(languageKeys, englishLanguage, webJsonSourceKey, LanguageJsonType.WEB),
                this.languageJsonUpdate(languageKeys, englishLanguage, cmsJsonSourceKey, LanguageJsonType.CMS),
                this.languageJsonUpdate(languageKeys, englishLanguage, vendorJsonSourceKey, LanguageJsonType.VENDOR)

            ]);
            languageKeys[LanguageJsonType.BACKEND] = allJson[0];
            languageKeys[LanguageJsonType.USER] = allJson[1];
            languageKeys[LanguageJsonType.DELIVERY] = allJson[2];
            languageKeys[LanguageJsonType.WEB] = allJson[3];
            languageKeys[LanguageJsonType.CMS] = allJson[4];
            languageKeys[LanguageJsonType.VENDOR] = allJson[5];
            // console.log("languageKeys", JSON.stringify(languageKeys))
            await this.updateLanguage(languageKeys._id, languageKeys);
        }
        return true;
    }
}
