import { Controller, Query, Get, Logger } from '@nestjs/common';
import { LanguageService } from './language.service';
import { ResponseLanguageCMSDetailsDTO } from './language.model';


import { ApiUseTags, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { UtilService } from '../auth/util.service';
import { LanguageJsonType, CommonResponseModel, ResponseErrorMessage } from '../auth/app.model';

@Controller('languages')
@ApiUseTags('Languages')
export class LanguageController {
    private logger = new Logger(LanguageController.name)
    constructor(
        private languageService: LanguageService,
        private utilService: UtilService
    ) {
    }






    @Get('/cms')
    @ApiOperation({ title: 'Get language for cms' })
    @ApiResponse({ status: 200, description: 'Return list', type: ResponseLanguageCMSDetailsDTO })
    @ApiResponse({ status: 404, description: 'Unauthorized or Not found', type: ResponseErrorMessage })
    public async getLanguageCms(@Query() query): Promise<CommonResponseModel> {
        try {
            let language, code = query.code, languageData = {};
            if (code) language = await this.languageService.getLanguageByCode(code);
            else language = await this.languageService.getDefaultLanguage();

            if (language) {
                languageData[language['languageCode']] = language[LanguageJsonType.CMS];
                return this.utilService.successResponseData(languageData);
            } else {
                language = await this.languageService.getDefaultLanguage();
                languageData[language['languageCode']] = language[LanguageJsonType.CMS];
                return this.utilService.successResponseData(languageData);
            }
        } catch (e) {
            this.utilService.errorResponse(e);
        }
    }


}
