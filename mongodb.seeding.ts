import * as mongoose from 'mongoose';
const readline = require('readline');

import { CommentSchema } from "./src/comments/comments.model";


const CommentCollections = require('./seedings/comment');


export class SeedDatabase {
    private Comment = mongoose.model<any>('Comment', CommentSchema);


    private production = false;
    private connectionUrl = "";

    constructor() {
        console.log("Do you want to reset database.?")
        console.log("It will delete all data from your database. Be careful while running this.");

        if (process.env.MONGO_DB_URL) {
            this.connectionUrl = process.env.MONGO_DB_URL;
            if (process.env.SEEDING_TYPE && process.env.SEEDING_TYPE == 'production') this.production = true;
            this.connect();
        } else {
            const consoleInterface = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });
            consoleInterface.question("\x1b[36m%s\x1b[0mEnter the Mongo DB connection uri string: ", (url) => {
                this.connectionUrl = url;

                consoleInterface.question("\x1b[36m%s\x1b[0mIs it for production? (production/staging):", (input) => {
                    if (input == "production") this.production = true;
                    console.log("\nPRODUCTION: ", this.production);
                    consoleInterface.close();
                });
            });
            consoleInterface.on('close', () => {
                this.connect();
            });
        }
    }

    connect() {
        if (this.connectionUrl) {
            mongoose.connect(this.connectionUrl, { useUnifiedTopology: true, useNewUrlParser: true }, (err) => {
                if (err) {
                    console.log("\x1b[31mCOULD NOT RESET DATABASE");
                    console.log(err.message);
                    process.exit(0);
                } else {
                    this.reset();
                }
            });
        } else {
            console.log("\x1b[31m NO CONNECTION URL FOUND");
        }
    }

    async reset() {
        console.log("-------------------START-------------------");
        await this.deleteCollections();


        if (!this.production) await this.setOthers();
        console.log("-------------------FINISHED-------------------");
        console.log("\nNow you can exit.");
        process.exit(0);
    }

    async deleteCollections() {
        await this.Comment.deleteMany({});
        console.log("Deleted Collection: Comment");


    }



    async setOthers() {
        await this.Comment.insertMany(CommentCollections);
        console.log("Added Collection: Comment");


    }




}

const reptile = new SeedDatabase();